use log::{debug, error, info, warn};
use regex::RegexBuilder;
use std::fs::{self, File};
use std::io::{self, Write};
use std::path::{Path, PathBuf};
use std::time::Instant;

use crate::utils;

#[derive(Debug)]
pub struct Replacer {
    dest_file: PathBuf,
    working_file: PathBuf,
    case_insensitive: bool,
    dry_run: bool,
}

impl Replacer {
    pub fn new(dest: PathBuf) -> Self {
        Self {
            working_file: into_working_file(&dest),
            dest_file: dest,
            case_insensitive: false,
            dry_run: false,
        }
    }

    pub fn enable_dry_run(&mut self, dry_run: bool) -> &mut Self {
        self.dry_run = dry_run;
        self
    }

    pub fn enable_case_insensitive(&mut self, case_insensitive: bool) -> &mut Self {
        self.case_insensitive = case_insensitive;
        self
    }

    // Replaces searched string pattern by replacement in the specified file.
    // Returns the number of lines replaced.
    pub fn replace_string_all(&self, pattern: &str, replacement: &str) -> io::Result<usize> {
        if self.case_insensitive {
            self.do_replace_all(Box::new(|text: &String| -> String {
                utils::string::ireplace_all(text.as_str(), pattern, replacement)
            }))
        } else {
            self.do_replace_all(Box::new(|text: &String| -> String {
                utils::string::replace_all(text.as_str(), pattern, replacement)
            }))
        }
    }

    // Replaces searched regular expression pattern by replacement in the specified file.
    // Returns the number of lines replaced.
    pub fn replace_regex_all(&self, pattern: &str, replacement: &str) -> io::Result<usize> {
        let regex = RegexBuilder::new(pattern)
            .case_insensitive(self.case_insensitive)
            .build()
            .unwrap();

        self.do_replace_all(Box::new(|text: &String| -> String {
            regex.replace_all(text, replacement).to_string()
        }))
    }

    fn do_replace_all<F>(&self, replace: F) -> io::Result<usize>
    where
        F: Fn(&String) -> String,
    {
        let now = Instant::now();
        let mut nb_lines_replaced: usize = 0;

        debug!("Create {:?}", self.working_file);
        let mut dest_file = File::create(self.working_file.as_path())?;

        debug!("Read {:?}", self.dest_file);
        for line in utils::file::read_lines(self.dest_file.as_path())? {
            match line {
                Ok(l) => {
                    let replaced = replace(&l);

                    if !self.dry_run {
                        writeln!(dest_file, "{}", replaced)?;
                    }

                    if l != replaced {
                        nb_lines_replaced += 1;
                    }
                }
                Err(err) => {
                    warn!("Abort {:?} processing, {}", self.dest_file, err);
                    return Err(err);
                }
            }
        }

        if nb_lines_replaced >= 1 {
            debug!("Rename {:?} as {:?}", self.working_file, self.dest_file);
            if !self.dry_run {
                fs::rename(self.working_file.as_path(), self.dest_file.as_path())?;
            }
        }

        info!(
            "Replaced {} line(s) in {:?}, {:?}",
            nb_lines_replaced,
            self.dest_file,
            now.elapsed()
        );

        Ok(nb_lines_replaced)
    }

    fn clean_residual_files(&self) {
        if self.working_file.exists() {
            match fs::remove_file(self.working_file.as_path()) {
                Ok(_) => debug!("Removed {:?}", self.working_file),
                Err(err) => error!("Could not remove {:?}, {}", self.working_file, err),
            }
        }
    }
}

impl Drop for Replacer {
    fn drop(&mut self) {
        self.clean_residual_files();
    }
}

// Add extra extension to current file
fn into_working_file(file_path: &Path) -> PathBuf {
    let dest_file_path = format!("{}.part", file_path.display());
    let mut path = PathBuf::new();
    path.push(dest_file_path);

    path
}

#[cfg(test)]
mod tests {
    use super::*;

    use assert_fs::prelude::*;

    #[test]
    fn test_replace_string_all() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("sample.txt")?;
        file.write_str("A test\nActual content\nMore content\nAnother test\n")?;

        let replacer = Replacer::new(file.to_path_buf());

        let result = replacer.replace_string_all("not_present_in_file", "whatever");
        assert_eq!(result.unwrap(), 0, "number of lines replaced");

        let result = replacer.replace_string_all("[cC]ontent$", "whatever");
        assert_eq!(result.unwrap(), 0, "number of lines replaced");

        let result = replacer.replace_string_all("content", "content replaced");
        assert_eq!(result.unwrap(), 2, "number of lines replaced");
        assert_eq!(
            fs::read_to_string(file.path()).unwrap(),
            "A test\nActual content replaced\nMore content replaced\nAnother test\n"
        );

        Ok(())
    }

    #[test]
    fn test_replace_string_all_case_insensitive() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("sample.txt")?;
        file.write_str("A test\nActual content\nMore content\nAnother test\n")?;

        let mut replacer = Replacer::new(file.to_path_buf());
        replacer.enable_case_insensitive(true);

        let result = replacer.replace_string_all("CoNTeNT", "content replaced");
        assert_eq!(result.unwrap(), 2, "number of lines replaced");
        assert_eq!(
            fs::read_to_string(file.path()).unwrap(),
            "A test\nActual content replaced\nMore content replaced\nAnother test\n"
        );

        Ok(())
    }

    #[test]
    fn test_replace_regex_all() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("sample.txt")?;
        file.write_str("A test\nActual content\nMore content\nAnother test\n")?;

        let replacer = Replacer::new(file.to_path_buf());

        let result = replacer.replace_regex_all("not_present_in_file", "whatever");
        assert_eq!(result.unwrap(), 0, "number of lines replaced");

        let result = replacer.replace_regex_all("[cC]ontent$", "content replaced");
        assert_eq!(result.unwrap(), 2, "number of lines replaced");
        assert_eq!(
            fs::read_to_string(file.path()).unwrap(),
            "A test\nActual content replaced\nMore content replaced\nAnother test\n"
        );

        let result = replacer.replace_regex_all("^.+$", "line replaced");
        assert_eq!(result.unwrap(), 4, "number of lines replaced");
        assert_eq!(
            fs::read_to_string(file.path()).unwrap(),
            "line replaced\nline replaced\nline replaced\nline replaced\n"
        );

        Ok(())
    }

    #[test]
    fn test_replace_regex_all_case_insensitive() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("sample.txt")?;
        file.write_str("A test\nActual content\nMore content\nAnother test\n")?;

        let mut replacer = Replacer::new(file.to_path_buf());
        replacer.enable_case_insensitive(true);

        let result = replacer.replace_regex_all("[cC]oNTeNT$", "content replaced");

        assert_eq!(result.unwrap(), 2, "number of lines replaced");
        assert_eq!(
            fs::read_to_string(file.path()).unwrap(),
            "A test\nActual content replaced\nMore content replaced\nAnother test\n"
        );

        Ok(())
    }

    #[test]
    fn test_enable_dry_run() -> Result<(), Box<dyn std::error::Error>> {
        let content = "A test\nActual content\nMore content\nAnother test\n";
        let file = assert_fs::NamedTempFile::new("sample.txt")?;
        file.write_str(content)?;

        let mut replacer = Replacer::new(file.to_path_buf());
        replacer.enable_dry_run(true);

        let result = replacer.replace_string_all("content", "content replaced");
        assert_eq!(result.unwrap(), 2, "number of lines replaced");
        assert_eq!(fs::read_to_string(file.path()).unwrap(), content);

        Ok(())
    }

    #[test]
    fn test_clean_residual_files() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("sample.txt")?;
        file.write_str("A test\n")?;

        let working_file = into_working_file(file.path());

        let replacer = Replacer::new(file.to_path_buf());
        assert!(!working_file.exists());

        let result = replacer.replace_string_all("not_present_in_file", "nothing");
        assert_eq!(result.unwrap(), 0);
        assert!(working_file.exists());

        replacer.clean_residual_files();
        assert!(!working_file.exists());

        Ok(())
    }

    #[test]
    fn test_into_working_file() {
        assert_eq!(
            into_working_file(Path::new("my_file.txt")),
            PathBuf::from("my_file.txt.part")
        );
        assert_eq!(
            into_working_file(Path::new("folder/my_file.txt")),
            PathBuf::from("folder/my_file.txt.part")
        );
        assert_eq!(
            into_working_file(Path::new("folder1/folder2/my_file.txt")),
            PathBuf::from("folder1/folder2/my_file.txt.part")
        );
    }
}
