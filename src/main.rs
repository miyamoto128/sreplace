use clap::Parser;
use colored::Colorize;
use log::info;
use std::path::PathBuf;
use std::process;
use utils::file::{follow_symlinks, has_extension};

mod core;
mod utils;

///
#[derive(Parser, Clone)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// The string to look for
    pattern: String,
    /// The replacement string
    replacement: String,
    /// The path to the file or directory
    path: std::path::PathBuf,

    /// Search pattern as a regular expression (slower)
    #[arg(short = 'e', long)]
    regex: bool,

    /// Case insensitive search
    #[arg(short = 'i', long)]
    case_insensitive: bool,

    /// Replace only the files with the specified extension
    #[arg(short = 't', long)]
    extension: Option<String>,

    /// Follow symbolic links
    #[arg(short = 'L', long)]
    follow_symlink: bool,

    /// Do not modify any file, just print what would happen
    #[arg(short, long)]
    dry_run: bool,

    #[clap(flatten)]
    verbose: clap_verbosity_flag::Verbosity,
}

fn main() {
    let args = Cli::parse();

    if args.dry_run {
        println!(
            "{}",
            "Dry-run mode enabled, no file will be modified"
                .yellow()
                .bold()
        );
    }

    env_logger::Builder::new()
        .filter_level(args.verbose.log_level_filter())
        .init();

    let result_code = if args.path.is_dir() {
        replace_in_dir(args)
    } else {
        replace_in_regular_file(&args.path, &args)
    };

    process::exit(result_code);
}

fn replace_in_regular_file(dest_path: &PathBuf, args: &Cli) -> i32 {
    let mut current_path = dest_path.clone();

    if dest_path.is_symlink() {
        if !args.follow_symlink {
            eprintln!("Skip {:?}, expect regular file", dest_path);
            return exitcode::USAGE;
        }

        current_path = follow_symlinks(dest_path);
        info!(
            "follow symbolic link from {:?} to {:?}",
            &dest_path, &current_path
        );
    }

    if let Some(matching_extension) = args.extension.clone() {
        if !has_extension(current_path.as_path(), &matching_extension) {
            println!(
                "Skip {:?}, does not match extension {:?}",
                dest_path, matching_extension
            );
            return exitcode::OK;
        }
    }

    let mut replacer = core::Replacer::new(current_path);
    replacer
        .enable_dry_run(args.dry_run)
        .enable_case_insensitive(args.case_insensitive);

    let result = if args.regex {
        replacer.replace_regex_all(&args.pattern, &args.replacement)
    } else {
        replacer.replace_string_all(&args.pattern, &args.replacement)
    };

    match result {
        Ok(0) => println!("No replacement in {:?}", dest_path),
        Ok(1) => println!("Replaced 1 line in {:?}", dest_path),
        Ok(nb_lines_replaced) => {
            println!("Replaced {} lines in {:?}", nb_lines_replaced, dest_path)
        }
        Err(err) => {
            eprintln!("Error occured processing {:?}, {}", dest_path, err);
            return err.raw_os_error().unwrap_or(exitcode::IOERR);
        }
    }

    exitcode::OK
}

fn replace_in_dir(args: Cli) -> i32 {
    info!("Recursively process directory {:?}", args.path);

    use std::sync::mpsc::channel;
    use threadpool::ThreadPool;

    let pool = ThreadPool::new(utils::thread::max_threads());
    let (tx, rx) = channel();

    let mut overall_result_code = exitcode::OK;
    let mut count: usize = 0;
    let matching_extension = args.extension.clone().unwrap_or_default();

    println!("Using {} worker(s)", pool.max_count());

    for entry in walkdir::WalkDir::new(args.path.clone())
        .follow_links(args.follow_symlink)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.file_type().is_file())
        .filter(|e| has_extension(e.path(), matching_extension.as_str()))
    {
        let tx = tx.clone();
        let args = args.clone();

        pool.execute(move || {
            let result_code = replace_in_regular_file(&entry.into_path(), &args);
            tx.send(result_code).expect("enough channels")
        });
        count += 1;
    }

    for result_code in rx.iter().take(count) {
        if result_code != exitcode::OK && overall_result_code == exitcode::OK {
            overall_result_code = result_code;
        }
    }

    let msg = match count {
        0 => String::from("No file replaced"),
        1 => String::from("Processed 1 file"),
        n => format!("Processed {} files", n),
    };
    println!("{}", msg.bold());

    overall_result_code
}

#[cfg(test)]
mod tests {
    use assert_cmd::prelude::*;
    use assert_fs::prelude::*;
    use predicates::prelude::*;
    use std::fs;
    use std::process::Command;

    #[test]
    fn replace_strings() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("sample.txt")?;
        file.write_str("Please me,\nreplace me\n")?;

        let mut cmd = Command::cargo_bin("sr")?;

        cmd.arg("replace me").arg("replaced").arg(file.path());
        cmd.assert()
            .success()
            .stdout(predicate::str::contains("Replaced 1 line"));

        assert_eq!(
            fs::read_to_string(file.path()).unwrap(),
            "Please me,\nreplaced\n"
        );
        Ok(())
    }

    #[test]
    fn replace_regex() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("sample.txt")?;
        file.write_str("Please me,\nreplace me\n")?;

        let mut cmd = Command::cargo_bin("sr")?;

        cmd.arg("--regex").arg("me,?$").arg("ME!").arg(file.path());
        cmd.assert()
            .success()
            .stdout(predicate::str::contains("Replaced 2 lines"));

        assert_eq!(
            fs::read_to_string(file.path()).unwrap(),
            "Please ME!\nreplace ME!\n"
        );
        Ok(())
    }

    #[test]
    fn no_replacement() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("sample.txt")?;
        file.write_str("Please me,\nreplace me\n")?;

        let mut cmd = Command::cargo_bin("sr")?;

        cmd.arg("unknown_pattern").arg("whatever").arg(file.path());
        cmd.assert()
            .success()
            .stdout(predicate::str::contains("No replacement"));

        assert_eq!(
            fs::read_to_string(file.path()).unwrap(),
            "Please me,\nreplace me\n"
        );
        Ok(())
    }

    #[test]
    fn file_doesnt_exist() -> Result<(), Box<dyn std::error::Error>> {
        let mut cmd = Command::cargo_bin("sr")?;

        cmd.arg("searched")
            .arg("replaced")
            .arg("test/file/doesnt/exist");
        cmd.assert()
            .failure()
            .stderr(predicate::str::contains("Error occured"));

        Ok(())
    }

    #[test]
    fn replace_in_dir() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("folder1/folder2/folder3/sample.txt")?;
        file.write_str("Please me,\nreplace me\n")?;

        let mut cmd = Command::cargo_bin("sr")?;

        cmd.arg("replace me")
            .arg("replaced")
            .arg(file.parent().unwrap());
        cmd.assert()
            .success()
            .stdout(predicate::str::contains("Processed 1 file"));

        assert_eq!(
            fs::read_to_string(file.path()).unwrap(),
            "Please me,\nreplaced\n"
        );
        Ok(())
    }

    #[test]
    fn matching_extension() -> Result<(), Box<dyn std::error::Error>> {
        let file = assert_fs::NamedTempFile::new("folder1/folder2/folder3/sample.txt")?;
        file.write_str("Please me,\nreplace me\n")?;

        let mut cmd = Command::cargo_bin("sr")?;
        cmd.arg("--extension")
            .arg("md")
            .arg("replace me")
            .arg("replaced")
            .arg(file.parent().unwrap());
        cmd.assert()
            .success()
            .stdout(predicate::str::contains("No file replaced"));

        let mut cmd = Command::cargo_bin("sr")?;
        cmd.arg("--extension")
            .arg("txt")
            .arg("replace me")
            .arg("replaced")
            .arg(file.parent().unwrap());
        cmd.assert()
            .success()
            .stdout(predicate::str::contains("Replaced 1 line"));

        Ok(())
    }
}
