#[must_use = "returns the replaced string as a new allocation, \
                  without modifying the original"]
pub fn replace_all(text: &str, pattern: &str, replacement: &str) -> String {
    if text.is_empty() || pattern.is_empty() {
        return String::from(text);
    }

    let mut result = String::with_capacity(text.len() * 2);
    let mut subject = text;

    while let Some(offset) = subject.find(pattern) {
        let (first, last) = subject.split_at(offset);

        result.push_str(first);
        result.push_str(replacement);
        (_, subject) = last.split_at(pattern.len());
    }
    result.push_str(subject);

    result
}

// Case insensitive replace
#[must_use = "returns the replaced string as a new allocation, \
                  without modifying the original"]
pub fn ireplace_all(text: &str, pattern: &str, replacement: &str) -> String {
    if text.is_empty() || pattern.is_empty() {
        return String::from(text);
    }

    let mut result = String::with_capacity(text.len() * 2);

    let mut subject = text;
    let mut isubject = text.to_lowercase();
    let ipattern = pattern.to_lowercase();

    while let Some(offset) = isubject.find(&ipattern) {
        let (first, last) = subject.split_at(offset);

        result.push_str(first);
        result.push_str(replacement);
        (_, subject) = last.split_at(pattern.len());

        let (_, mut last) = isubject.split_at(offset);
        (_, last) = last.split_at(ipattern.len());
        isubject = last.to_string();
    }
    result.push_str(subject);

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_replace_all() {
        assert_eq!(replace_all("Hello world!", "world", "all"), "Hello all!");
        assert_eq!(replace_all("abc abc abc", "c ", "cDE "), "abcDE abcDE abc");
        assert_eq!(
            replace_all("Hello world!", "no_match", "all"),
            "Hello world!"
        );

        // unicode
        assert_eq!(replace_all("Hello world!", "world", "世界"), "Hello 世界!");
        assert_eq!(replace_all("Hello 世界!", "世界", "world"), "Hello world!");

        // case sensitive
        assert_eq!(replace_all("Hello world!", "WORLD", "all"), "Hello world!");
        assert_eq!(replace_all("Hello world!", "worLd", "all"), "Hello world!");

        // empty strings
        assert_eq!(replace_all("Hello world!", "", "all"), "Hello world!");
        assert_eq!(replace_all("", "no_match", "all"), "");
        assert_eq!(replace_all("Hello world!", " world", ""), "Hello!");
    }

    #[test]
    fn test_ireplace_all() {
        assert_eq!(ireplace_all("Hello world!", "world", "all"), "Hello all!");
        assert_eq!(ireplace_all("abc abc abc", "c ", "cDE "), "abcDE abcDE abc");
        assert_eq!(
            ireplace_all("Hello world!", "no_match", "all"),
            "Hello world!"
        );

        // unicode
        assert_eq!(ireplace_all("Hello world!", "world", "世界"), "Hello 世界!");
        assert_eq!(ireplace_all("Hello 世界!", "世界", "world"), "Hello world!");

        // case insensitive
        assert_eq!(ireplace_all("Hello world!", "WORLD", "all"), "Hello all!");
        assert_eq!(ireplace_all("Hello world!", "worLd", "all"), "Hello all!");

        // empty strings
        assert_eq!(ireplace_all("Hello world!", "", "all"), "Hello world!");
        assert_eq!(ireplace_all("", "no_match", "all"), "");
        assert_eq!(ireplace_all("Hello world!", " world", ""), "Hello!");
    }

    #[test]
    #[allow(unused)]
    fn bench_replace_all_native() {
        assert_eq!(
            "Hello 世界, hello world, hello all, hello, hello!".replace("hello", "bonjour"),
            "Hello 世界, bonjour world, bonjour all, bonjour, bonjour!"
        );

        for _i in 0..1000000 {
            "Hello 世界, hello world, hello all, hello, hello!".replace("hello", "bonjour");
        }
    }

    #[test]
    #[allow(unused)]
    fn bench_replace_all_home_made() {
        assert_eq!(
            replace_all(
                "Hello 世界, hello world, hello all, hello, hello!",
                "hello",
                "bonjour"
            ),
            "Hello 世界, bonjour world, bonjour all, bonjour, bonjour!"
        );

        for _i in 0..1000000 {
            replace_all(
                "Hello 世界, hello world, hello all, hello, hello!",
                "hello",
                "bonjour",
            );
        }
    }

    #[test]
    #[allow(unused)]
    fn bench_ireplace_all_home_made() {
        assert_eq!(
            ireplace_all(
                "Hello 世界, hello world, hello all, hello, hello!",
                "Hello",
                "bonjour"
            ),
            "bonjour 世界, bonjour world, bonjour all, bonjour, bonjour!"
        );

        for _i in 0..1000000 {
            ireplace_all(
                "Hello 世界, hello world, hello all, hello, hello!",
                "Hello",
                "bonjour",
            );
        }
    }
}
