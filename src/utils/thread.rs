#[must_use = "returns an estimate of the maximum number of threads available for this program"]
pub fn max_threads() -> usize {
    match std::thread::available_parallelism() {
        Ok(num) => num.get(),
        _ => 1,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_max_threads() {
        assert!(max_threads() >= 1);
    }
}
