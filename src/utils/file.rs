use std::fs::{self, File};
use std::io::{self, BufRead};
use std::path::{Path, PathBuf};

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
// From https://doc.rust-lang.org/stable/rust-by-example/std_misc/file/read_lines.html#efficient-method
#[must_use = "returns an iterator over the lines of a file"]
pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

#[must_use = "returns symlink destination path if any, source path otherwise"]
pub fn follow_symlinks(src_path: &Path) -> PathBuf {
    if src_path.is_symlink() {
        if let Ok(dest_path) = fs::read_link(src_path) {
            let mut result = PathBuf::from(src_path.parent().unwrap());
            result.push(dest_path);

            return follow_symlinks(result.as_path());
        }
    }
    PathBuf::from(src_path)
}

#[must_use = "returns true for matching extension, false otherwise"]
pub fn has_extension(path: &Path, maching_extension: &str) -> bool {
    if maching_extension.is_empty() {
        return true;
    }

    if let Some(extension) = path.extension() {
        return extension.to_string_lossy() == maching_extension;
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_has_extension() {
        assert!(has_extension(Path::new("file.txt"), "txt"));
        assert!(has_extension(Path::new("folder/folder/file.txt"), "txt"));

        assert!(has_extension(Path::new("file.txt"), ""));
        assert!(has_extension(Path::new(""), ""));

        assert!(!has_extension(Path::new("file.txt"), "yml"));
        assert!(!has_extension(Path::new("file.txt"), "TXT"));
        assert!(!has_extension(Path::new(""), "md"));
    }
}
