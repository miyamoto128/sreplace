# sreplace

A simple to use, efficient, and full-featured command line string replacer.

Sed like command line tool that aims to be simple and fast to use.

This is a rewritting of a [former project of mine](https://sourceforge.net/projects/sreplace/), from C++ to rust.

## Installation

1. [install rust](https://www.rust-lang.org/tools/install)
1. install `sreplace` using `cargo`:
```sh
cargo install sreplace
```

The ready to use executable command is `sr`.

## Usage

```html
sr <PATTERN> <REPLACEMENT> <PATH>

Arguments:
  <PATTERN>      The string to look for
  <REPLACEMENT>  The replacement string
  <PATH>         The path to the file or directory
```

## Examples

```shell
$ cat hello.txt
Hello world!

$ sr world all hello.txt
Replaced 1 line in "hello.txt"

$ cat hello.txt
Hello all!
```

### Replace in regular file
![Replace in regular file](/doc/sr-regular.gif "sr hello bonjour hello.txt")

### Replace recursively in folder
![Replace in folder](/doc/sr-folder.gif "sr --dry-run hello bonjour .")

## Features

* UTF-8 encoding support
* Support regular expression searched patterns
* Support case insensitive searched patterns
* Filter on regular files extensions
* Recursively browse directories
* Concurrently process multiple files

## License

GPL v3
